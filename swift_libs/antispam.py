from __future__ import annotations

from collections import namedtuple
from datetime import datetime, timedelta
from typing import List, Hashable, Tuple, Optional, Dict, Iterable, Awaitable, Union, Callable

import discord
from discord.ext.commands import BucketType, Context
from redbot.core import commands
from redbot.core.utils.antispam import AntiSpam as _AntiSpam, Interval, AntiSpamInterval

__all__ = ("AntiSpamCheck",)
MockCooldown = namedtuple("Cooldown", ["rate", "per", "type"])


class _AntiSpamStamp:
    __slots__ = ("checks",)

    def __init__(self, *checks: AntiSpam):
        self.checks: List[AntiSpam] = list(checks)

    def __bool__(self):
        return bool(self.checks)

    def __iter__(self) -> Iterable[AntiSpam]:
        yield from self.checks

    def __iadd__(self, other: AntiSpam) -> _AntiSpamStamp:
        if not isinstance(other, AntiSpam):
            if isinstance(other, _AntiSpam):
                raise TypeError("The given AntiSpam object is not derived from our subclass")
            raise TypeError(f"Cannot add object of type {type(other)!r}")
        return type(self)(*self.checks, other)

    def __call__(self) -> None:
        for check in self.checks:
            check.stamp()


class AntiSpam(_AntiSpam):
    def _valid_timestamps(self, interval: AntiSpamInterval):
        return [t for t in self.__event_timestamps if (t + interval.period) > datetime.utcnow()]

    def interval_cooldowns(self) -> Dict[AntiSpamInterval, Optional[timedelta]]:
        """Get the time remaining for an interval to no longer consider an action spammy"""
        now = datetime.utcnow()
        intervals = {}
        for interval in self.__intervals:
            timestamps = self._valid_timestamps(interval)
            if len(timestamps) >= interval.frequency:
                intervals[interval] = interval.period - (now - min(timestamps))
            else:
                intervals[interval] = None
        return intervals

    def time_left(self) -> Optional[Tuple[AntiSpamInterval, timedelta]]:
        """Get how long a user must wait before attempting to perform the same action again"""
        intervals = self.interval_cooldowns()
        if not any(intervals.values()):
            return None
        return tuple(
            max([(k, v) for k, v in intervals.items() if v is not None], key=lambda x: x[1])
        )


class AntiSpamCheck:
    """Cooldown-like utility built on top of :class:`redbot.core.utils.antispam.AntiSpam`

    Arguments
    ---------
    bucket: commands.BucketType
        The bucket which this check should count towards. A bucket type of either
        ``guild``, ``member``, or ``category`` assumes this check should only be used in
        guild contexts, and will accordingly raise a ``NoPrivateMessage`` error in
        non-guild contexts.
    intervals: List[Interval]
        A list of AntiSpam compatible intervals. If not specified, the default antispam intervals
        will be used.

    Keyword Arguments
    -----------------
    only_if: Callable[[Context], Union[bool, Awaitable[bool]]]
        Predicate to call when checking if this check should be applied to the current invocation.
        If this returns :obj:`False`, the current invocation is considered to be exempt
        from this check.
    """

    def __init__(
        self,
        bucket: BucketType,
        intervals: List[Interval] = ...,
        *,
        only_if: Callable[[Context], Union[bool, Awaitable[bool]]] = None,
    ):
        self.bucket = bucket
        self.intervals = intervals or []
        self._anti_spam = {}
        self.only_if = only_if

    def clone(self):
        return type(self)(bucket=self.bucket, intervals=self.intervals)

    def _new_antispam(self):
        return AntiSpam(self.intervals)

    # noinspection PyRedundantParentheses
    def bucket_key(self, ctx: commands.Context) -> Hashable:
        if (
            self.bucket in (BucketType.guild, BucketType.member, BucketType.category)
            and not ctx.guild
        ):
            raise commands.NoPrivateMessage()

        if self.bucket is BucketType.default:
            return "__GLOBAL__"
        elif self.bucket is BucketType.guild:
            return ("GUILD", ctx.guild.id)
        elif self.bucket is BucketType.channel:
            return ("CHANNEL", ctx.channel.id)
        elif self.bucket is BucketType.member:
            return ("MEMBER", ctx.guild.id, ctx.author.id)
        elif self.bucket is BucketType.user:
            return ("USER", ctx.author.id)
        elif self.bucket is BucketType.category:
            return ("CATEGORY", ctx.channel.category_id or ctx.guild.id)
        else:
            raise RuntimeError(f"Unknown bucket type {self.bucket!r}")

    def _get_antispam(self, ctx: commands.Context) -> AntiSpam:
        bkey = self.bucket_key(ctx)
        if bkey in self._anti_spam:
            antispam = self._anti_spam[bkey]
        else:
            antispam = self._anti_spam[bkey] = self._new_antispam()
        return antispam

    def check(self, maybe_cmd=None):
        # noinspection PyUnresolvedReferences
        """Apply the current AntiSpamCheck as a command check

        This adds a ``stamp`` method to your command's ``ctx`` variable, which is used to mark
        successful invocations towards the anti-spam criteria.

        Multiple checks may be applied to the same command; calling ``ctx.stamp()`` will apply
        to all applied checks.

        Example
        --------
        >>> from redbot.core import commands
        >>> from swift_libs import AntiSpamCheck
        >>> # Custom intervals may also be specified as a second argument with the same
        >>> # arguments that AntiSpam would regularly expect.
        >>> @AntiSpamCheck(commands.BucketType.user)
        ... @commands.command()
        ... def my_command(ctx):
        ...     ctx.stamp()
        """

        async def predicate(ctx: commands.Context):
            # Always add this property, so that even if this is the only check and the
            # current invocation bypasses this antispam check, the given command can always call
            # ctx.stamp() without having to add handling for this specific case.
            if not hasattr(ctx, "stamp"):
                ctx.stamp = _AntiSpamStamp()

            if self.only_if and not await discord.utils.maybe_coroutine(self.only_if, ctx):
                return True

            antispam = self._get_antispam(ctx)
            is_spammy = antispam.time_left()
            if is_spammy:
                interval, time_left = is_spammy
                raise commands.CommandOnCooldown(
                    MockCooldown(rate=interval.frequency, per=interval.period, type=self.bucket),
                    time_left.total_seconds(),
                )

            ctx.stamp += antispam
            return True

        if maybe_cmd is not None:
            return commands.check(predicate)(maybe_cmd)
        else:
            return commands.check(predicate)

    __call__ = check
