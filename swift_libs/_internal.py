import logging

from redbot.core import Config

from .i18n import Translator

log = logging.getLogger("red.swift_libs")
translate = Translator(__file__, strict=True)

try:
    # We don't currently store data in Config, but instead use it solely for
    # determining which driver we're using in `[p]swiftlibs debuginfo`.
    config = Config.get_conf(None, cog_name="swift_libs", identifier=413574221325)
except (RuntimeError, TypeError):
    # allow loading in an environment without red running, such as in the REPL or external scripts
    config = None
