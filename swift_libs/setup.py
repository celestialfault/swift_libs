from __future__ import annotations

import asyncio
import importlib
import sys
import warnings
from asyncio import Future
from collections import Counter
from contextlib import suppress
from datetime import datetime
from typing import Type, Union, Set, List, Optional

from redbot.core import commands, checks, version_info as red_version
from redbot.core.bot import Red
from redbot.core.utils import deduplicate_iterables
from redbot.core.utils.chat_formatting import inline, box

from . import __version__ as _sl_version, version_info as _sl_ver_info
from ._internal import log, translate as translate_, config
from .checks import is_dev, sl_version
from .menus import confirm, PaginatedMenu
from .time import FutureTime

translate = translate_.group("util_cog")
__all__ = ("setup_cog", "get_instance")
loaded = {}
# Set of cog names that depend on swift_libs, used when '[p]swiftlibs reload' is invoked.
# Cogs in this set are not guaranteed to still be loaded.
dependents: Set[str] = set()
# Cogs that've requested a newer version of swift_libs than what is currently in memory,
# and as such require swift_libs to be reloaded before they can be loaded again.
# These are guaranteed to be unloaded.
require_update: Set[str] = set()


def maybe_get(package, var, default=None):
    from importlib import import_module

    try:
        mod = import_module(package)
    except ImportError:
        return default
    else:
        return getattr(mod, var, default)


# noinspection PyPep8Naming,PyProtectedMember
@translate.cog("help.cog")
class swift_libs(commands.Cog):
    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot
        # Keep a copy of our current version for logging purposes
        self.version = _sl_ver_info
        self.counter = Counter()
        # 3.1.x compatibility
        if hasattr(bot, "counter"):
            self.counter.update(**bot.counter)

    @commands.Cog.listener()
    async def on_message(self, _):
        self.counter["messages_read"] += 1

    @commands.Cog.listener()
    async def on_command(self, _):
        self.counter["processed_commands"] += 1

    @property
    def messages_read(self):
        return self.counter.get("messages_read", 0)

    @property
    def processed_commands(self):
        return self.counter.get("processed_commands", 0)

    @property
    def uptime(self) -> datetime:
        # This previously was a workaround to red#2967, but was rendered redundant by red#2976.
        warnings.warn("swift_libs.uptime is deprecated; use Red.uptime instead", DeprecationWarning)
        return self.bot.uptime

    def sl_reload_hook(self, old: swift_libs):
        log.debug(
            "Reloading swift_libs (old version: %r, new version: %r)",
            getattr(old, "version", "unknown"),
            self.version,
        )
        with suppress(AttributeError):
            self.counter.update(old.counter)

    @staticmethod
    def dump(prefix: str):
        dumped = 0
        for k in list(sys.modules.keys()):
            if k.startswith(prefix):
                dumped += 1
                del sys.modules[k]
        return dumped

    @commands.group(name="swiftlibs")
    @checks.is_owner()
    @translate.command("help.swiftlibs._root")
    async def swiftlibs(self, ctx):
        pass

    @swiftlibs.command(name="debuginfo", hidden=True)
    @translate.command("help.swiftlibs.info")
    @commands.check(lambda ctx: ctx.bot.get_command("debuginfo") is not None)
    async def sl_info(self, ctx: commands.Context):
        # Force plain text output; we're (ab)using the fact that you can await Future objects,
        # and that we can set their result ahead of time.
        f = Future()
        f.set_result(False)
        ctx.embed_requested = lambda *_, **__: f

        # Grab a copy of the original send context method so we can grab the output of [p]debuginfo
        # and append our own data onto it
        send = ctx.send
        debug_info: List[str] = []

        async def send_override(content=None, *, embed=None, **_):
            if embed:
                log.warning("Somehow caught an embed when calling debuginfo; this shouldn't occur.")
            if content is not None:
                debug_info.append(content)

        # Closed course, professional driver. Don't attempt this on your own.
        try:
            ctx.send = send_override
            await ctx.invoke(self.bot.get_cog("Core").debuginfo)
        except Exception:
            ctx.send = send
            if debug_info:
                for line in debug_info:
                    await ctx.send(line)
            raise
        else:
            ctx.send = send

        debug_info = [x.replace("```", "").rstrip() for x in debug_info]
        versions = {
            "swift_libs": _sl_version,
            "swift_i18n": maybe_get("swift_i18n.meta", "__version__", "unknown"),
            "asyncpg": maybe_get("asyncpg", "__version__", "N/A"),
        }

        # Red 3.2 and up removed the Mongo driver, so this is only useful on 3.0 through 3.1
        if red_version < VersionInfo(major=3, minor=2, micro=0, releaselevel="final"):
            versions["motor"] = maybe_get("motor", "version", "N/A")

        debug_info.extend(
            [
                f"Current Config driver: {type(config.driver).__qualname__}",
                f"Is development mode enabled: {dev_mode(self.bot)}",
                *[f"{k} version: {v}" for k, v in versions.items()],
            ]
        )
        await ctx.send(box("\n".join(debug_info)))

    @swiftlibs.command(name="dump")
    @is_dev()
    @translate.command("help.swiftlibs.dump")
    async def sl_dump(self, ctx: commands.Context, module_prefix: str):
        await ctx.send(translate("dumped", count=self.dump(module_prefix)))

    @swiftlibs.command(name="reload")
    @translate.command("help.swiftlibs.reload")
    async def sl_reload(self, ctx: commands.Context, skip_confirmation: bool = False):
        # 'swift_libs.setup' -> 'swift_libs'
        self.dump(".".join(self.__module__.split(".")[:-1]))
        self.dump("swift_i18n")
        to_reload = [x.lower() for x in dependents if x in ctx.bot.cogs]
        to_reload.extend(map(lambda x: x.lower(), require_update))
        to_reload = deduplicate_iterables(to_reload)
        names = " ".join(inline(x) for x in to_reload)

        if skip_confirmation or await confirm(ctx, content=translate("reload_confirm", cogs=names)):
            await ctx.invoke(ctx.bot.get_cog("Core").reload, *to_reload)
        else:
            await ctx.send(translate("reload_manual", cogs=names))

    # The following commands are intentionally left untranslated.

    @swiftlibs.group(name="debug", i18n=lambda _: _)
    @is_dev()
    async def sl_debug(self, ctx):
        """swift_libs functionality debug commands"""
        pass

    @sl_debug.command(name="confirm", i18n=lambda _: _)
    async def sldbg_confirm(self, ctx: commands.Context):
        """Bare-bones confirmation prompt"""
        await ctx.send(str(await confirm(ctx, content="y/n?")))

    @sl_debug.command(name="pmenu", i18n=lambda _: _)
    async def sldbg_pmenu(self, ctx: commands.Context, pages: int = 5, timeout: int = 30):
        """Bare-bones paginated menu"""
        await PaginatedMenu(
            pages=[f"page {n}" for n in range(pages)],
            bot=ctx.bot,
            channel=ctx.channel,
            member=ctx.author,
        ).prompt(timeout=timeout)

    @sl_debug.command(name="translate", aliases=["trans", "transl"], i18n=lambda _: _)
    async def sldbg_transl(self, ctx: commands.Context, cog: str, key: str, *args):
        """Retrieve a translated string from a cog

        Example usage:

        > [p]swift_libs debug translate Overseer page current=1 total=1

        This would result in:

        > Page 1/1
        """
        from .i18n import Translator

        args = [arg.split("=") for arg in args if "=" in arg]
        args = {arg[0]: "=".join(arg[1:]) for arg in args if len(arg) >= 2}
        file = importlib.import_module(ctx.bot.get_cog(cog).__module__).__file__
        t = Translator(file)
        await ctx.send(t(key, **args))

    @sl_debug.command(name="time", i18n=lambda _: _)
    async def sldbg_time(self, ctx: commands.Context, *, duration: FutureTime):
        """Return what `duration` resolves to

        The given delta may return odd results on account of leap years, or when working around
        months with less days than the current month (such as January 30th to Feburary 28th).
        """
        from redbot.core.utils.chat_formatting import humanize_timedelta

        # noinspection PyTypeChecker
        await ctx.send(
            f"Resolved time: {datetime.utcnow() + duration}\n"
            f"Delta: {humanize_timedelta(timedelta=duration)}"
        )


def get_instance() -> Optional[swift_libs]:
    """Get the current swift_libs cog instance"""
    # TODO: Replace this function with something less disgusting
    # This was never originally meant to be public, but unfortunately has to be due to how
    # we're re-implementing message and processed command counters.
    return loaded.get("swift_libs")


def load_or_reload(bot: Red, load: Type[commands.Cog], *args, **kwargs):
    name = load.__name__
    if name not in loaded or loaded[name] is not bot.get_cog(name):
        old = bot.cogs.get(name)
        if old:
            log.debug("Unloading previously loaded version of internal cog %r", name)
            bot.remove_cog(name)
        log.info("Loading internal cog %r", name)
        loaded[name] = load = load(bot, *args, **kwargs)
        if old and hasattr(load, "sl_reload_hook"):
            load.sl_reload_hook(old)
        bot.add_cog(load)


def setup_cog(
    bot: Red,
    cog: Union[str, Type[commands.Cog], commands.Cog],
    *,
    require_version: str = None,
    **discard,
):
    """Setup the internal swift_libs cog and run pre-flight checks on the current environment

    Keyword arguments passed to this function that aren't recognized are discarded.

    This function returns an :class:`asyncio.Future` with a result of :obj:`None` for backwards
    compatibility for cogs that expect this to be awaitable.

    Arguments
    ---------
    bot: Red
        The current bot instance to add the swift_libs cog to
    cog: Union[str, Type[Cog], Cog]
        The cog to register as being loaded. Your cog name will be added to a global set,
        which will be used to determine which cogs to reload upon invocation of
        ``[p]swiftlibs reload``.
    require_version: Optional[str]
        If this is specified, swift_libs must match at least this specified version. If not,
        a CogLoadError is raised, instructing the bot owner to either reload swift_libs
        or restart their bot.

    Example
    -------
    >>> # if the bot is running 0.0.2a0, this will raise an informative error
    >>> # explaining how to update the version of swift_libs currently in memory
    >>> setup_cog(bot, "MyCog", require_version="0.0.3a0")
    """
    cog_name = (
        cog
        if isinstance(cog, str)
        else cog.__name__
        if isinstance(cog, type)
        else type(cog).__name__
    )

    if discard:
        log.warning(
            "Cog %r passed unrecognized setup keyword argument(s): %r",
            cog_name,
            ", ".join(repr(x) for x in discard.keys()),
        )

    if require_version is not None:
        sl_version(require_version, cog_name)

    load_or_reload(bot, swift_libs)
    dependents.add(cog_name)

    # Backwards compatibility for cogs that expect this to be awaitable
    f = asyncio.Future()
    f.set_result(None)
    return f
